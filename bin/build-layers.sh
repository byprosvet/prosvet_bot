#!/bin/bash

cd layers/gems
rm -rf build
mkdir -p build/ruby/gems
bundle install --path=build/ruby/gems
mv build/ruby/gems/ruby/* build/ruby/gems/
rm -rf build/ruby/gems/2.7.0/cache build/ruby/gems/ruby

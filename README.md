### Deploy
- install `docker` and `aws-sam-cli` (https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html)
- `./bin/build-layers.sh`
- `sam build`
- `sam deploy --guided` (for the first time, later you can run `sam deploy`)


### Local
#### Initial
- install `docker` and `aws-sam-cli` (https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html)
- `docker-compose up -d`
- `./bin/build-layers.sh`
- `sam build`
- `cp env.example.json env.json` and put there `TELEGRAM_BOT_TOKEN` of some bot for testing purposes
- download and run `ngrok` (`./ngrok http 3000`)
- `ruby app/scripts/set_bot_webhook.rb`
- `sam local start-api --debug --env-vars env.json --docker-network prosvet_bot_lambda-local`

Now you can test bot as usual bot via Telegram. Run `sam build` each time you change your code (no need to restart api).

#### Google Auth
- First step from https://developers.google.com/sheets/api/quickstart/ruby. Put `credentials.json` to `app/google_api/auth`
- The rest will be done by command from `Setup` section

#### Following
- `docker-compose up -d`
- `sam build`
- `sam local start-api --debug --env-vars env.json --docker-network prosvet_bot_lambda-local`
- `sam local invoke FetchDataFunction --debug --env-vars env.json --docker-network prosvet_bot_lambda-local` to run `FetchDataFunction`

#### DynamoDB GUI
- `npm install -g dynamodb-admin`
- `docker start dynamodb`
- `AWS_ACCESS_KEY_ID=defaultkey AWS_SECRET_ACCESS_KEY=defaultsecret AWS_REGION=us-east-1 DYNAMO_ENDPOINT=http://localhost:8000 dynamodb-admin`


### Setup
`(cd app && AWS_ACCESS_KEY_ID=defaultkey AWS_SECRET_ACCESS_KEY=defaultsecret AWS_REGION=us-east-1 DYNAMO_ENDPOINT=http://localhost:8000 GOOGLE_CREDENTIALS=put_json_with_your_google_credentials_here CREATE_TABLES=true SEED_DATA=true SETUP_GOOGLE_AUTH=true ruby scripts/setup.rb)`

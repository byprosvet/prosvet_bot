# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    telegram_id { 1234567 }
  end

  factory :newspaper do
    sequence(:title) { |n| "Честная газета #{n}" }
    search_title { title.downcase }
    active { true }
    locations { %w[Брестская Витебская Гомельская Гродненская Минск Минская Могилёвская] }
    description { 'Еженедельное издание о новостях протеста' }
    color { '#006d75' }
  end

  factory :newspaper_issue do
    newspaper { create(:newspaper) }
    sequence(:telegram_file_id) { |n| "BQACAgIAAxkIOOIak2AfoH5l0fbfS1qXC1_LmjoMI08fAAK4CgAC5gz4QAStUJ1teJrD#{n}" }
    number { rand(1..50) }
    date { [Date.today - 60 + number, Date.today].min }
    version { 1 }
  end
end

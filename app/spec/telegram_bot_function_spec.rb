# frozen_string_literal: true

describe '#telegram_bot_handler' do
  context '/start message' do
    let(:event) do
      {
        'body' => <<-JSON
          {
            "update_id": 805619282,
            "message": {
              "message_id": 66,
              "from": {
                "id": 136013947,
                "is_bot": false,
                "first_name": "Test",
                "username": "test_user",
                "language_code": "en"
              },
              "chat": {
                "id": 136013947,
                "first_name": "Test",
                "username": "test_user",
                "type": "private"
              },
              "date": 1605885121,
              "text": "/start"
            }
          }
        JSON
      }
    end
    let(:message_params) do
      {
        chat_id: 136013947,
        text: 'Переходите на канал @by_prosvet',
        parse_mode: 'HTML',
        disable_web_page_preview: true,
      }
    end
    let(:ok_response) { { statusCode: 200, body: '{"status":"ok"}' } }
    let(:api) { double('Telegram::Bot::Api') }

    before { allow(Telegram::Bot::Api).to receive(:new).and_return(api) }

    it do
      expect(api).to receive(:send_message).with(message_params)

      response = telegram_bot_handler(event: event, context: {})
      expect(response).to eq(ok_response)
    end
  end
end

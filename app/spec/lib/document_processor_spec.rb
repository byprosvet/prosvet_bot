# frozen_string_literal: true

RSpec.describe DocumentProcessor do
  describe '.call' do
    let(:add_to_map) { true }
    let(:newspaper) { Newspaper.create(title: 'Честная газета', add_to_map: add_to_map) }
    let(:newspaper_issue) { newspaper.newspaper_issues.create(number: 10, date: Date.new(2021, 2, 5)) }
    let(:user) { User.create(telegram_id: 1234567) }
    let(:chat_id) { 7654321 }
    let(:data) { { 'newspaper_issue_id' => newspaper_issue.id, 'telegram_chat_id' => chat_id } }
    let(:sender) { double('NewspaperIssueSender') }
    let(:publisher) { double('NewspaperIssuePublisher') }
    let(:api) { double('Telegram::Bot::Api') }
    let(:text) { '"Честная газета" (10) отправлен подписанным пользователям' }
    let(:message_params) do
      {
        chat_id: chat_id,
        text: text,
        parse_mode: 'HTML',
        disable_web_page_preview: true
      }
    end

    before do
      allow(Telegram::Bot::Api).to receive(:new).and_return(api)
      allow(api).to receive(:send_message)
      allow(api).to receive(:send_document)
      Subscription.create(user: user, newspaper: newspaper)
      allow(NewspaperIssueSender).to receive(:new).and_return(sender)
      allow(sender).to receive(:call)
      allow(NewspaperIssuePublisher).to receive(:new).and_return(publisher)
      allow(publisher).to receive(:call)
    end

    it 'sends document to subscribed users' do
      expect(NewspaperIssueSender).to receive(:new).with(newspaper_issue)
      expect(sender).to receive(:call)
      expect(api).to receive(:send_message).with(message_params)

      described_class.call(data)
    end

    context 'issue should be added to map' do
      let(:text) { '"Честная газета" (10) добавлен в таблицу' }

      it 'publishes issue' do
        expect(NewspaperIssuePublisher).to receive(:new).with(newspaper_issue)
        expect(sender).to receive(:call)
        expect(api).to receive(:send_message).with(message_params)

        described_class.call(data)
      end
    end

    context 'issue shoud not be added to map' do
      let(:add_to_map) { false }

      it 'does not publish issue' do
        expect(NewspaperIssuePublisher).not_to receive(:new)
        expect(api).not_to receive(:send_message).with(include(text: '"Честная газета" (10) добавлен в таблицу'))

        described_class.call(data)
      end
    end
  end
end

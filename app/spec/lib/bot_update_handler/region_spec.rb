# frozen_string_literal: true

RSpec.describe BotUpdateHandler::Region do
  let(:telegram_user_id) { 1234567 }
  let(:message_data) do
    {
      'message_id' => 8817,
      'from' => {
        'id' => telegram_user_id,
        'is_bot' => false,
        'first_name' => 'Anton',
        'username' => 'palchikov',
        'language_code' => 'en'
      },
      'chat' => {
        'id' => telegram_user_id,
        'first_name' => 'Anton',
        'username' => 'palchikov',
        'type' => 'private'
      },
      'date' => 1614319539,
      'text' => text
    }
  end
  let(:data) { { 'update_id' => 923441665, 'message' => message_data } }
  let(:handler) { described_class.new(data) }

  describe '#suitable?' do
    let(:subject) { handler.suitable? }

    context 'region message' do
      let(:text) { 'Минск' }

      it { is_expected.to eq(true) }
    end

    context 'all recent issues message' do
      let(:text) { 'Все последние выпуски' }

      it { is_expected.to eq(true) }
    end

    context 'other message' do
      let(:text) { 'Сообщение' }

      it { is_expected.to eq(false) }
    end
  end

  describe '#call' do
    def load_user(user_to_load = user)
      User.where(telegram_id: user_to_load.telegram_id).first
    end

    let!(:user) { create(:user, telegram_id: telegram_user_id, region: 'Минск') }
    let(:api) { double('Telegram::Bot::Api') }
    let(:action) { double('BotAction::SendNewspapers') }

    before do
      allow(Telegram::Bot::Api).to receive(:new).and_return(api)
      allow(DocumentProcessor).to receive(:add_to_queue)
      allow(api).to receive(:send_message)
      allow(api).to receive(:send_document)
      allow(BotAction::SendNewspapers).to receive(:new).and_return(action)
      allow(action).to receive(:call)
    end

    context 'region message' do
      let(:text) { 'Гомельская' }

      it 'updates user region' do
        expect { handler.call }.to change { load_user.region }.from('Минск').to('Гомельская')
      end

      it 'runs send newspapers action' do
        expect(action).to receive(:call).with('Гомельская')

        handler.call
      end
    end

    context 'all recent newspapers issues' do
      let(:text) { 'Все последние выпуски' }

      it 'does not update user region' do
        expect { handler.call }.not_to(change { load_user.region })
      end

      it 'runs send newspapers action with nil region value' do
        expect(action).to receive(:call).with(nil)

        handler.call
      end
    end
  end
end

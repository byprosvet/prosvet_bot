# frozen_string_literal: true

RSpec.describe BotUpdateHandler::SubscribeButton do
  let(:telegram_user_id) { 156380947 }
  let(:data) do
    {
      'update_id' => 850612620,
      'callback_query' => {
        'id' => '586597817920134323',
        'from' => {
          'id' => telegram_user_id,
          'is_bot' => false,
          'first_name' => 'John',
          'username' => 'johny',
          'language_code' => 'en'
        },
        'message' => {
          'message_id' => 682,
          'from' => {
            'id' => 1332343775,
            'is_bot' => true,
            'first_name' => 'Test Bot',
            'username' => 'random_test_bot'
          },
          'chat' => {
            'id' => telegram_user_id,
            'first_name' => 'John',
            'username' => 'johny',
            'type' => 'private'
          },
          'date' => 1614025427,
          'edit_date' => 1614026680,
          'document' => {
            'file_name' => 'честная_газета_20.pdf',
            'mime_type' => 'application/pdf',
            'thumb' => {
              'file_id' => 'AAMCAgADDQMAAgYqYDQS0-D6URpU95Ez2-OPdZOSTDQAAvcMAAKJa2hJO3Q40nsERMrCmTGgLgADAQAHbQADjjIAA',
              'file_unique_id' => 'AQADwpkitC5AA11yAAI',
              'file_size' => 17339,
              'width' => 226,
              'height' => 320
            },
            'file_id' => 'BQACAgIAAxkDAAICqmA0EtPg_UOaVPeRM9vj0nERkkw0AAL3DAACiWtoSTt0ONJ7BETKHgQ',
            'file_unique_id' => 'AgAD9wwAAolraEk',
            'file_size' => 12904303
          },
          'caption' => "Честная газета (выпуск 20)\nЕженедельное издание о новостях протеста",
          'caption_entities' => [{ 'offset' => 0, 'length' => 14, 'type' => 'bold' }],
          'reply_markup' => {
            'inline_keyboard' => [[
              { 'text' => 'Подписаться', 'callback_data' => 'subscribe_newspaperid' }
            ]]
          }
        },
        'chat_instance' => '-7025571664870473809',
        'data' => callback_query_data
      }
    }
  end
  let(:handler) { described_class.new(data) }
  let(:api) { double('Telegram::Bot::Api') }

  describe '#suitable?' do
    context 'callback query' do
      context 'subscribe' do
        let(:callback_query_data) { 'subscribe_adw8adhawd' }

        it { expect(handler.suitable?).to eq(true) }
      end

      context 'unsubscribe' do
        let(:callback_query_data) { 'unsubscribe_d8ad8hd82d' }

        it { expect(handler.suitable?).to eq(true) }
      end

      context 'random text' do
        let(:callback_query_data) { 'some_random_text' }

        it { expect(handler.suitable?).to eq(false) }
      end
    end

    context 'regular message' do
      let(:data) do
        {
          'update_id' => 923440923,
          'message' => {
            'message_id' => 1234,
            'from' => {
              'id' => telegram_user_id,
              'is_bot' => false,
              'first_name' => 'John',
              'username' => 'johny',
              'language_code' => 'en'
            },
            'chat' => {
              'id' => telegram_user_id,
              'first_name' => 'John',
              'username' => 'johny',
              'type' => 'private'
            },
            'date' => 1612685439,
            'text' => 'test test'
          }
        }
      end

      it { expect(handler.suitable?).to eq(false) }
    end
  end

  describe '#call' do
    before do
      allow(Telegram::Bot::Api).to receive(:new).and_return(api)
      allow(api).to receive(:answer_callback_query)
      allow(api).to receive(:edit_message_reply_markup)
    end

    let(:newspaper) { Newspaper.create(title: 'Честная газета') }
    let(:callback_query_data) { "subscribe_#{newspaper.id}" }

    it 'updates subscription' do
      expected_args = { action: 'subscribe', newspaper_id: newspaper.id,
                        user: have_attributes(telegram_id: telegram_user_id) }
      expect(Subscription::Update).to receive(:call).with(expected_args).and_call_original

      handler.call
    end

    it 'answers callback query' do
      expected_args = { callback_query_id: '586597817920134323', text: 'Подписка оформлена', show_alert: true }
      expect(api).to receive(:answer_callback_query).with(expected_args)

      handler.call
    end

    it 'updates reply markup' do
      expected_inline_keybord = [[have_attributes(text: 'Отписаться', callback_data: "unsubscribe_#{newspaper.id}")]]
      expected_args = {
        chat_id: telegram_user_id,
        message_id: 682,
        reply_markup: have_attributes(inline_keyboard: expected_inline_keybord)
      }
      expect(api).to receive(:edit_message_reply_markup).with(expected_args)

      handler.call
    end
  end
end

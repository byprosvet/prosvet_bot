# frozen_string_literal: true

RSpec.describe BotUpdateHandler::Document do
  describe '#suitable?' do
    let(:telegram_user_id) { 36054583 }
    let(:caption_data) do
      {
        'caption' => '/add_issue Протестный Гомель_120_25.02.2021',
        'caption_entities' => [{ 'offset' => 0, 'length' => 10, 'type' => 'bot_command' }]
      }
    end
    let(:message_data) do
      {
        'message_id' => 8817,
        'from' => {
          'id' => telegram_user_id,
          'is_bot' => false,
          'first_name' => 'Anton',
          'username' => 'palchikov',
          'language_code' => 'en'
        },
        'chat' => {
          'id' => telegram_user_id,
          'first_name' => 'Anton',
          'username' => 'palchikov',
          'type' => 'private'
        },
        'date' => 1614319539,
        'document' => {
          'file_name' => 'Формат_А4_Протестный_Гомель_№120.pdf',
          'mime_type' => 'application/pdf',
          'thumb' => {
            'file_id' => 'AAMCAgADGQEAAiJxYDiPs0AR03gkj6XO9OJJfulGbSsAAhIMAAK4HsFJeZ8Jh4E',
            'file_unique_id' => 'AQADUlwqwC4AA-ZSAAI',
            'file_size' => 44035,
            'width' => 320,
            'height' => 320
          },
          'file_id' => 'BQACAgIAAxkBAAIicWA4j7NAEdN4JI-lzvTiSX7pRm0rAAISDAACuB7BSXmfCZrkG1',
          'file_unique_id' => 'AgADEgwAAqwzaUk',
          'file_size' => 52918
        }
      }
    end
    let(:data) { { 'update_id' => 923441665, 'message' => message_data.merge(Hash(caption_data)) } }
    let(:handler) { described_class.new(data) }

    context 'document' do
      it { expect(handler.suitable?).to eq(true) }

      context 'without caption' do
        let(:caption_data) { nil }

        it { expect(handler.suitable?).to eq(false) }
      end

      context 'not bot_command' do
        let(:caption_data) { { caption: 'Протестный Гомель_120_25.02.2021' } }

        it { expect(handler.suitable?).to eq(false) }
      end

      context 'wrong bot_command' do
        let(:caption_data) do
          {
            'caption' => '/ads_issue Протестный Гомель_120_25.02.2021',
            'caption_entities' => [{ 'offset' => 0, 'length' => 10, 'type' => 'bot_command' }]
          }
        end

        it { expect(handler.suitable?).to eq(false) }
      end

      context 'edited message' do
        let(:data) { { 'update_id' => 923441665, 'edited_message' => message_data.merge(Hash(caption_data)) } }

        it { expect(handler.suitable?).to eq(true) }
      end
    end

    context 'regular message' do
      let(:data) do
        {
          'update_id' => 923440923,
          'message' => {
            'message_id' => 1234,
            'from' => {
              'id' => telegram_user_id,
              'is_bot' => false,
              'first_name' => 'John',
              'username' => 'johny',
              'language_code' => 'en'
            },
            'chat' => {
              'id' => telegram_user_id,
              'first_name' => 'John',
              'username' => 'johny',
              'type' => 'private'
            },
            'date' => 1612685439,
            'text' => 'test test'
          }
        }
      end

      it { expect(handler.suitable?).to eq(false) }
    end
  end

  describe '#call' do
    let(:chat_id) { 5678910 }
    let(:caption) { '/add_issue Честная газета_30_07.02.2021' }
    let(:data) do
      {
        'update_id' => 923440923,
        'message' => {
          'message_id' => 1234,
          'from' => {
            'id' => chat_id,
            'is_bot' => false,
            'first_name' => 'John',
            'username' => 'johny',
            'language_code' => 'en'
          },
          'chat' => {
            'id' => chat_id,
            'first_name' => 'John',
            'username' => 'johny',
            'type' => 'private'
          },
          'date' => 1612685439,
          'document' => {
            'file_name' => 'Честная_газета_30.pdf',
            'mime_type' => 'application/pdf',
            'thumb' => {
              'file_id' =>
                'AAMCAgADGQEAAhqTYB-iuyXR9t9LWpcLX9eaOgwjTx8AArgKAALmDPhI0i1QnW14koPkPf-fLgADAQAHbQADc90AAh4E',
              'file_unique_id' => 'AQAD5D3_ny4AA3NPAAI',
              'file_size' => 33381,
              'width' => 320,
              'height' => 320
            },
            'file_id' => 'BQACAgIAAxkIOOIak2AfoH5l0fbfS1qXC1_LmjoMI08fAAK4CgAC5gz4QAStUJ1teJrDHgQ',
            'file_unique_id' => 'AgLKuApAAuYM-Ef',
            'file_size' => 79_296
          },
          'caption' => caption,
          'caption_entities' => [{ 'offset' => 0, 'length' => 10, 'type' => 'bot_command' }]
        }
      }
    end
    let(:add_to_map) { true }
    let(:title) { 'Честная газета' }
    let!(:newspaper) { Newspaper.create(title: title, search_title: title.downcase, add_to_map: add_to_map) }
    let(:subject) { described_class.new(data).call }
    let(:api) { double('Telegram::Bot::Api') }
    let(:newspaper_issue) { Newspaper.where(id: newspaper.id).first.newspaper_issues.first }
    let(:message_params) do
      {
        chat_id: chat_id,
        text: text,
        parse_mode: 'HTML',
        disable_web_page_preview: true
      }
    end

    before do
      allow(Telegram::Bot::Api).to receive(:new).and_return(api)
      allow(DocumentProcessor).to receive(:add_to_queue)
      allow(api).to receive(:send_message)
    end

    context 'params are correct' do
      context 'and newspaper exists' do
        it 'creates newspaper issue with correct attributes' do
          expect { subject }
            .to change { Newspaper.where(id: newspaper.id).first.newspaper_issues.count }
            .from(0)
            .to(1)
          expect(newspaper_issue.attributes)
            .to include(
              number: 30,
              date: Date.new(2021, 2, 7),
              version: 1,
              telegram_file_id: 'BQACAgIAAxkIOOIak2AfoH5l0fbfS1qXC1_LmjoMI08fAAK4CgAC5gz4QAStUJ1teJrDHgQ',
              file_name: 'Честная_газета_30.pdf'
            )
        end

        context 'and issue should be added to map' do
          let(:add_to_map) { false }

          let(:text) do
            "Номер добавлен: \"Честная газета\", выпуск 30, 2021-02-07, v1\n\n"\
            'Добавление номера в таблицу и/или рассылка подписанным пользователям в процессе...'
          end

          it 'schedules document processing' do
            expect(DocumentProcessor).to receive(:add_to_queue).with(String, chat_id)

            subject
          end

          it 'sends status as telegram message' do
            expect(api).to receive(:send_message).with(message_params)

            subject
          end
        end
      end
    end

    context 'params are not correct' do
      let(:caption) { '/add_issue Честная гозета_30_07.02.2021' }
      let(:text) { 'Нет такой газеты или указаны не все данные' }

      it 'does not create newspaper issue with attributes' do
        expect { subject }.not_to(change { Newspaper.where(id: newspaper.id).first.newspaper_issues.count })
      end

      it 'does not schedule document processing' do
        expect(DocumentProcessor).not_to receive(:add_to_queue)

        subject
      end

      it 'sends status as telegram message' do
        expect(api).to receive(:send_message).with(message_params)

        subject
      end
    end
  end
end

# frozen_string_literal: true

RSpec.describe Newspaper do
  let!(:newspapers) { create_list(:newspaper, 3) }
  let!(:inactive_newspapers) { create_list(:newspaper, 3, active: false) }
  let!(:brest_newspapers) { create_list(:newspaper, 3, locations: %w[Брестская]) }
  let!(:gomel_newspapers) { create_list(:newspaper, 3, locations: %w[Гомельская]) }

  describe '.by_region' do
    subject { described_class.by_region(region).to_a }

    let(:region) { 'Брестская' }
    let(:expected_newspapers) { newspapers + brest_newspapers }

    it { is_expected.to match_array(expected_newspapers) }
  end

  describe '.active' do
    subject { described_class.active.to_a }

    let(:expected_newspapers) { newspapers + brest_newspapers + gomel_newspapers }

    it { is_expected.to match_array(expected_newspapers) }
  end

  describe '.search' do
    subject { described_class.search(query) }

    let(:query) { 'Витебский вестник' }
    let!(:newspaper) { create(:newspaper, title: 'Витебский Вестник') }

    it { is_expected.to eq(newspaper) }
  end

  describe '.newspaper_issues_data' do
    let(:newspaper_issues_data) { described_class.newspaper_issues_data(region: region) }
    let(:newspapers_with_issues) do
      [
        newspapers.sample,
        gomel_newspapers.sample,
        brest_newspapers.sample,
        inactive_newspapers.sample
      ]
    end

    let!(:newspaper_issues) do
      newspapers_with_issues.map { |newspaper| create(:newspaper_issue, newspaper: newspaper) }
    end

    before do
      newspapers_with_issues.each do |newspaper|
        create_list(:newspaper_issue, 3, newspaper: newspaper, date: Date.today - rand(61..80))
      end
    end

    context 'region specified' do
      let(:region) { 'Гомельская' }
      let(:expected_data) do
        newspaper_issues.take(2).map do |issue|
          { newspaper: have_attributes(id: issue.newspaper.id), issue: have_attributes(id: issue.id) }
        end
      end

      it 'returns data for region' do
        expect(newspaper_issues_data).to match_array(expected_data)
      end
    end

    context 'region is not specified' do
      let(:region) { nil }
      let(:expected_data) do
        newspaper_issues.take(3).map do |issue|
          { newspaper: have_attributes(id: issue.newspaper.id), issue: have_attributes(id: issue.id) }
        end
      end

      it 'returns data for all newspapers' do
        expect(newspaper_issues_data).to match_array(expected_data)
      end
    end
  end
end

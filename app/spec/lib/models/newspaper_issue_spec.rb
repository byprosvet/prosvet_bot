# frozen_string_literal: true

RSpec.describe NewspaperIssue do
  describe '#add_to_map?' do
    let(:params) { { add_to_map: add_to_map } }
    let(:newspaper) { Newspaper.create(title: 'Честная газета', **params) }
    let!(:newspaper_issue) { newspaper.newspaper_issues.create }

    context 'true' do
      let(:add_to_map) { true }

      it { expect(newspaper_issue.add_to_map?).to eq(true) }
    end

    context 'false' do
      let(:add_to_map) { false }

      it { expect(newspaper_issue.add_to_map?).to eq(false) }
    end
  end
end

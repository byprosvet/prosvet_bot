# frozen_string_literal: true

RSpec.describe Subscription::Update do
  describe '.call' do
    let(:newspaper) { Newspaper.create(title: 'Честная газета') }
    let(:user) { User.create(telegram_id: 1234567) }
    let(:newspaper_id) { newspaper.id }

    subject { described_class.call(action: action, newspaper_id: newspaper_id, user: user) }

    context 'subscribe' do
      let(:action) { :subscribe }

      it { is_expected.to eq(result: :subscribed, subscription: Subscription.all.first) }

      context 'when newspaper does not exist' do
        let(:newspaper_id) { 'random-id' }

        it { is_expected.to eq(result: :error, subscription: nil) }
      end

      context 'when subscription already exists' do
        let!(:subscription) { Subscription.create(user: user, newspaper: newspaper) }

        it { is_expected.to eq(result: :error, subscription: subscription) }
      end
    end

    context 'unsubscribe' do
      let(:action) { :unsubscribe }

      context 'when subscription exists' do
        before { Subscription.create(user: user, newspaper: newspaper) }

        it { is_expected.to eq(result: :unsubscribed, subscription: nil) }
      end

      context 'when subscription does not exist' do
        it { is_expected.to eq(result: :error, subscription: nil) }
      end
    end

    context 'uknown action' do
      let(:action) { :unknown_action }

      it 'raises error' do
        expect { subject }.to raise_error(Subscription::Update::UnknownSubscriptionAction)
      end
    end
  end
end

# frozen_string_literal: true

RSpec.describe NewspaperIssueSender do
  let(:api) { double('Telegram::Bot::Api') }
  let(:newspaper) { Newspaper.create(title: 'Честная газета') }
  let(:newspaper_issue) do
    NewspaperIssue.create(
      newspaper: newspaper,
      number: 20,
      date: Date.new(2021, 2, 10),
      telegram_file_id: 'AAMCAgADDQMAAgYqYDQS0-D6URpU95Ez2-OPdZOSTDQAAvcMAAKJa2hJO3',
      file_name: 'Честная газета 20.pdf'
    )
  end
  let(:sender) { described_class.new(newspaper_issue) }
  let(:telegram_user_ids) { [123456, 123457, 123458] }

  before do
    telegram_user_ids.each do |telegram_id|
      Subscription.create(newspaper: newspaper, user: User.create(telegram_id: telegram_id))
    end
  end

  def expect_send_document(chat_id) # rubocop:disable Metrics/MethodLength
    expect(api)
      .to receive(:send_document)
      .with(
        chat_id: chat_id,
        document: 'AAMCAgADDQMAAgYqYDQS0-D6URpU95Ez2-OPdZOSTDQAAvcMAAKJa2hJO3',
        caption: start_with('<strong>Честная газета</strong> (выпуск 20 – 10.02.2021)'),
        parse_mode: 'HTML',
        disable_web_page_preview: true,
        reply_markup: have_attributes(
          inline_keyboard: [[have_attributes(text: 'Отписаться', callback_data: "unsubscribe_#{newspaper.id}")]]
        )
      )
  end

  describe '#call' do
    before do
      allow(Telegram::Bot::Api).to receive(:new).and_return(api)
      allow(api).to receive(:send_document)
    end

    it 'sends newspaper issue to each subscribed user' do
      telegram_user_ids.each { |chat_id| expect_send_document(chat_id) }

      sender.call
    end

    context 'user is deactivated' do
      let(:deactivated_user_id) { telegram_user_ids[0] }
      let(:telegram_response_error) do
        body = { ok: false, error_code: 403, description: 'Forbidden: user is deactivated' }.to_json
        response = OpenStruct.new(body: body)

        Telegram::Bot::Exceptions::ResponseError.new(response)
      end

      before do
        allow(api)
          .to receive(:send_document)
            .with(hash_including(chat_id: deactivated_user_id)) {
                raise telegram_response_error, 'Telegram API has returned the error.'
              }
      end

      it 'sends newspaper issue to each subscribed user' do
        telegram_user_ids.each { |chat_id| expect_send_document(chat_id) }

        sender.call
      end

      it 'unsubscribes deactivated user' do
        expect { sender.call }
          .to change {
            Subscription.where(
              'user_ids.contains' => deactivated_user_id,
              'newspaper_ids.contains' => newspaper.id
            ).count
          }
          .from(1)
          .to(0)
      end
    end
  end
end

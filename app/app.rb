# frozen_string_literal: true

require 'net/http'
require 'geocoder'
require 'telegram/bot'
require 'aws-sdk-sqs'

Dir['initializers/*.rb'].each { |file| require_relative file }
Dir['lib/models/*.rb'].each { |file| require_relative file }
Dir['lib/models/**/*.rb'].each { |file| require_relative file }
Dir['lib/file_service/*.rb'].each { |file| require_relative file }
Dir['lib/bot_action/*.rb'].each { |file| require_relative file }

require_relative 'lib/bot_update_handler/base'
require_relative 'lib/bot_update_handler/city_selection_button'
require_relative 'lib/bot_update_handler/document'
require_relative 'lib/bot_update_handler/location'
require_relative 'lib/bot_update_handler/region'
require_relative 'lib/bot_update_handler/simple_reply'
require_relative 'lib/bot_update_handler/stub_reply'
require_relative 'lib/bot_update_handler/want_help_button'
require_relative 'lib/bot_update_handler/subscribe_button'

require_relative 'lib/google_api/spreadsheets_helper'
require_relative 'lib/newspaper_issue_publisher'
require_relative 'lib/newspaper_issue_sender'
require_relative 'lib/telegram_bot_helper'
require_relative 'lib/telegram_update_handler'
require_relative 'lib/document_processor'

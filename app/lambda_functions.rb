# frozen_string_literal: true

require_relative 'app'

def telegram_bot_handler(event:, context:) # rubocop:disable Lint/UnusedMethodArgument
  data = get_data(event['body'])
  puts "data: #{data}"

  TelegramUpdateHandler.call(data)

  { statusCode: 200, body: { status: :ok }.to_json }
end

def document_process_handler(event:, context:) # rubocop:disable Lint/UnusedMethodArgument
  data = get_data(event['Records'][0]['body'])
  puts "process_document: #{data}"

  DocumentProcessor.call(data)

  { statusCode: 200, body: { status: :ok }.to_json }
end

def get_data(data)
  data.is_a?(Hash) ? data : JSON.parse(data)
end

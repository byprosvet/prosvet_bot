# frozen_string_literal: true

module DocumentProcessor
  DOCUMENTS_PROCESS_QUEUE_URL = ENV['DOCUMENTS_PROCESS_QUEUE_URL']

  module_function

  def call(data)
    issue = NewspaperIssue.find(data['newspaper_issue_id'])
    bot = TelegramBotHelper.new(data['telegram_chat_id'])

    publish_and_notify(issue, bot) if issue.add_to_map?
    send_out_and_notify(issue, bot)
  end

  def add_to_queue(newspaper_issue_id, telegram_chat_id)
    data = { newspaper_issue_id: newspaper_issue_id, telegram_chat_id: telegram_chat_id }
    params = {
      queue_url: DOCUMENTS_PROCESS_QUEUE_URL,
      message_body: data.to_json
    }

    Aws::SQS::Client.new.send_message(params)
  end

  def publish_and_notify(issue, bot)
    NewspaperIssuePublisher.new(issue).call
    bot.notify_issue_status(issue: issue, message: :published)
  end

  def send_out_and_notify(issue, bot)
    NewspaperIssueSender.new(issue).call
    bot.notify_issue_status(issue: issue, message: :sent_out)
  end
end

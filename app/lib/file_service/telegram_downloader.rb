# frozen_string_literal: true

module FileService
  module TelegramDownloader
    module_function

    def call(telegram_file_id, file_name)
      telegram_file = TelegramBotHelper.api.get_file(file_id: telegram_file_id)
      url = download_file_url(telegram_file['result']['file_path'])

      result = Net::HTTP.get(URI(url))
      file = File.new("/tmp/#{file_name}", 'w+')
      file.write(result)

      file
    end

    def download_file_url(file_path)
      telegram_bot_token = TelegramBotHelper::TELEGRAM_BOT_TOKEN

      "https://api.telegram.org/file/bot#{telegram_bot_token}/#{file_path}"
    end
  end
end

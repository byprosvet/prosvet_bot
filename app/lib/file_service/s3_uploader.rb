# frozen_string_literal: true

require 'aws-sdk-s3'

module FileService
  class S3Uploader
    BUCKET_NAME = ENV['DOCUMENTS_BUCKET_NAME']
    DEFAULT_ACL = 'public-read'
    DOCUMENTS_FOLDER = 'documents'

    def initialize(bucket_name: BUCKET_NAME)
      @s3 = Aws::S3::Resource.new
      @bucket = @s3.bucket(bucket_name)
    end

    def upload(file, **options)
      default_key = "#{DOCUMENTS_FOLDER}/#{File.basename(file)}"
      key = options.delete(:key) || default_key

      object = @bucket.object(key)
      object.put({ body: file, acl: DEFAULT_ACL }.merge(options))

      object
    rescue Aws::S3::Errors::InvalidAccessKeyId => e
      raise unless ENV['AWS_SAM_LOCAL']

      puts "Error while uploading file to S3: #{e.message}"
    end
  end
end

# frozen_string_literal: true

module TelegramUpdateHandler
  module_function

  def call(data)
    BotUpdateHandler::Base.build(data)&.call
  end
end

# frozen_string_literal: true

module BotUpdateHandler
  class Document < Base
    def suitable?
      !!message&.document && %r{/add_issue}.match?(message.caption)
    end

    def call
      result = create_issue
      schedule_document_processing(result[:issue]) unless result[:error]
      bot.notify_issue_status(result)
    end

    private

    def create_issue
      attrs = newspaper_issue_attributes
      newspaper = find_newspaper
      issue = find_existing_newspaper_issue(newspaper, attrs)

      return { error: :missing_data } if message_data.size < 3
      return { error: :newspaper_not_found } if newspaper.nil?
      return { error: :cant_parse_date } if attrs[:date].nil?
      return { error: :already_exists, issue: issue } if issue

      issue = newspaper.newspaper_issues.create(attrs)

      { issue: issue, message: :processing }
    end

    def schedule_document_processing(newspaper_issue)
      DocumentProcessor.add_to_queue(newspaper_issue.id, message.chat.id)
    end

    def newspaper_issue_attributes
      issue_number, date_str, version = message_data[1..3]

      {
        date: parse_date(date_str),
        number: issue_number[/\d+/]&.to_i,
        version: (version.to_s[/\d+/].presence || 1),
        telegram_file_id: message.document.file_id,
        file_name: message.document.file_name
      }.compact
    end

    def find_newspaper
      newspaper_title = message_data[0]

      Newspaper.search(newspaper_title) if newspaper_title
    end

    def find_existing_newspaper_issue(newspaper, attrs)
      search_attrs = attrs.slice(:date, :number, :version)

      return unless newspaper
      return unless search_attrs.values.all?

      newspaper.newspaper_issues.where(search_attrs).first
    end

    def message_data
      @message_data ||= message.caption.gsub(%r{/add_issue}, '').strip.split('_')
    end

    def parse_date(date_str)
      Date.parse(date_str.to_s)
    rescue Date::Error
      nil
    end
  end
end

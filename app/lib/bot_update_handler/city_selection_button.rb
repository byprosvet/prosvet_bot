# frozen_string_literal: true

module BotUpdateHandler
  class CitySelectionButton < Base
    def suitable?
      message&.text == I18n.t('buttons.select_city')
    end

    def call
      reply_with_regions_menu
    end

    private

    def reply_with_regions_menu
      text = I18n.t('texts.select_city')
      buttons = I18n.t('buttons.regions') + [I18n.t('buttons.menu')]
      keyboard = buttons.each_slice(2).to_a

      bot.show_reply_keyboard(text, keyboard)
    end
  end
end

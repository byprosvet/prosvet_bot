# frozen_string_literal: true

module BotUpdateHandler
  class SubscribeButton < Base
    def suitable?
      callback_data.match?(/^(un)?subscribe_/)
    end

    def call
      action, newspaper_id = callback_data.split('_')

      result = Subscription::Update.call(action: action, newspaper_id: newspaper_id, user: user)

      answer_callback_query(result[:result])
      update_reply_markup(result[:subscription], newspaper_id)
    end

    private

    def callback_data
      callback_query&.data.to_s
    end

    def answer_callback_query(result)
      bot.answer_callback_query(
        callback_query_id: callback_query.id,
        text: I18n.t(result, scope: :callback_query),
        show_alert: true
      )
    rescue Telegram::Bot::Exceptions::ResponseError => e
      raise unless e.message.match?('query is too old')
    end

    def update_reply_markup(subscription, newspaper_id)
      bot.edit_message_reply_markup(
        chat_id: bot.chat_id,
        message_id: callback_query.message.message_id,
        reply_markup: bot.subscribe_reply_markup(subscription, newspaper_id)
      )
    end
  end
end

# frozen_string_literal: true

module BotUpdateHandler
  class Region < Base
    def suitable?
      region_message? || all_message?
    end

    def call
      region = message.text if region_message?

      update_user_region(region) if region
      BotAction::SendNewspapers.new(bot).call(region)
    end

    private

    def all_message?
      message&.text == I18n.t('buttons.all_recent_issues')
    end

    def region_message?
      I18n.t('buttons.regions').include?(message&.text)
    end
  end
end

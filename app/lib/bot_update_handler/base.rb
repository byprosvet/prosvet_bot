# frozen_string_literal: true

module BotUpdateHandler
  class Base
    HANDLER_CLASSES = %w[StubReply SimpleReply WantHelpButton SubscribeButton CitySelectionButton Region Location Document].freeze

    attr_reader :message, :callback_query

    def self.build(data)
      return if data.blank?

      HANDLER_CLASSES.each do |key|
        handler = Object.const_get("BotUpdateHandler::#{key}").new(data)
        return handler if handler.suitable?
      end

      nil
    end

    def initialize(data)
      @update = Telegram::Bot::Types::Update.new(data)
      @message = @update.message || @update.edited_message
      @callback_query = @update.callback_query
    end

    private

    def bot
      @bot ||= begin
        msg = @message || @callback_query&.message

        TelegramBotHelper.new(msg.chat.id) if msg
      end
    end

    def user
      @user ||= User.find_or_create(@update.current_message.from.id)
    end

    def update_user_region(region)
      user.update_attributes(region: region)
    end
  end
end

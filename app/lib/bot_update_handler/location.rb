# frozen_string_literal: true

module BotUpdateHandler
  class Location < Base
    def suitable?
      !!message&.location
    end

    def call
      region = get_region(message.location)

      if region
        update_user_region(region)
        BotAction::SendNewspapers.new(bot).call(region)
      else
        send_error_message
      end
    end

    private

    def get_region(location)
      coordinates = [location.latitude, location.longitude]
      data = Geocoder.search(coordinates, language: :ru)[0].data
      address = data['address']

      (address['state'] || address['city']).split[0] if valid_address?(address)
    end

    def valid_address?(address)
      !!address && address['country'] == 'Беларусь'
    end

    def send_error_message
      bot.send_message(text: I18n.t('errors.wrong_location'))
    end
  end
end

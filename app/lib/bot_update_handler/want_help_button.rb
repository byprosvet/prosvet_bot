# frozen_string_literal: true

module BotUpdateHandler
  class WantHelpButton < Base
    def suitable?
      [I18n.t('buttons.want_help'), I18n.t('buttons.actual')].include?(message&.text)
    end

    def call
      reply_with_location_request
    end

    private

    def reply_with_location_request
      text = I18n.t('texts.location')
      keyboard = [
        [{ text: I18n.t('buttons.send_location'), request_location: true }],
        [I18n.t('buttons.select_city')],
        [I18n.t('buttons.all_recent_issues')],
        [I18n.t('buttons.menu')]
      ]

      bot.show_reply_keyboard(text, keyboard)
    end
  end
end

# frozen_string_literal: true

module BotUpdateHandler
  class StubReply < Base
    def suitable?
      true
    end

    def call
      bot.send_message(text: I18n.t('texts.go_to_channel'))
    end
  end
end

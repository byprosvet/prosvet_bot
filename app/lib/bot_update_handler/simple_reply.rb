# frozen_string_literal: true

module BotUpdateHandler
  class SimpleReply < Base
    REPLIES_MAP = {
      '/start' => { text_key: 'start', button_keys: %w[want_help advice contact report_map] },
      I18n.t('buttons.menu') => { text_key: 'menu', button_keys: %w[want_help advice contact donate report_map] },
      I18n.t('buttons.advice') => { text_key: 'advice', button_keys: %w[what_next menu] },
      I18n.t('buttons.report_map') => { text_key: 'map', button_keys: %w[want_help menu] },
      I18n.t('buttons.map') => { text_key: 'map', button_keys: %w[menu] },
      I18n.t('buttons.contact') => { text_key: 'contact', button_keys: %w[want_help menu] },
      I18n.t('buttons.what_next') => { text_key: 'groups', button_keys: %w[group_1 group_2 group_3] },
      I18n.t('buttons.group_1') => { text_key: 'group_1', button_keys: %w[actual menu] },
      I18n.t('buttons.group_2') => { text_key: 'group_2', button_keys: %w[actual menu] },
      I18n.t('buttons.group_3') => { text_key: 'group_3', button_keys: %w[actual menu] },
      I18n.t('buttons.donate') => { text_key: 'donate', button_keys: %w[menu] }
    }.freeze

    def suitable?
      REPLIES_MAP.keys.include?(message&.text)
    end

    def call
      reply_with_message_and_menu
    end

    private

    def reply_with_message_and_menu
      menu_params = REPLIES_MAP[message.text].fetch_values(:text_key, :button_keys)

      bot.show_menu(*menu_params)
    end
  end
end

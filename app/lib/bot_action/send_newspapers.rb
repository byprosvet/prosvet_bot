# frozen_string_literal: true

module BotAction
  class SendNewspapers
    attr_reader :bot

    def initialize(bot)
      @bot = bot
    end

    def call(region)
      data = Newspaper.newspaper_issues_data(region: region)

      if data.empty?
        send_error_message
      else
        send_info_message
        send_newspapers(data)
        send_info_message_with_keyboard
      end
    end

    def send_newspaper(entry)
      newspaper, issue = entry.values_at(:newspaper, :issue)
      subscription = Subscription.search(user_id: bot.chat_id, newspaper_id: newspaper.id)

      bot.send_document(
        document: issue.telegram_file_id,
        caption: caption(newspaper, issue),
        reply_markup: bot.subscribe_reply_markup(subscription, newspaper.id)
      )
    end

    private

    def caption(newspaper, issue)
      I18n.t(
        'texts.document_caption',
        title: newspaper.title,
        number: issue.number,
        description: newspaper.description,
        date: I18n.l(issue.date)
      )
    end

    def send_newspapers(data)
      data.map.with_index { |entry, _index| send_newspaper(entry) }
    end

    def send_error_message
      bot.send_message(text: I18n.t('errors.no_newspapers'))
    end

    def send_info_message
      bot.send_message(text: I18n.t('texts.newspapers_before'))
    end

    def send_info_message_with_keyboard
      bot.send_message(
        text: I18n.t('texts.newspapers_after'),
        reply_markup: bot.reply_keyboard_for(%w[map menu])
      )
    end
  end
end

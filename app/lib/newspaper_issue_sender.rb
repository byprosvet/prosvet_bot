# frozen_string_literal: true

class NewspaperIssueSender
  def initialize(newspaper_issue)
    @newspaper_issue = newspaper_issue
    @newspaper = newspaper_issue.newspaper
  end

  def call
    subscriptions.each { |subscription| send_document(subscription.user_telegram_id) }
  end

  private

  def subscriptions
    Subscription.where('newspaper_ids.contains' => @newspaper.id)
  end

  def send_document(user_telegram_id)
    bot = TelegramBotHelper.new(user_telegram_id)

    BotAction::SendNewspapers.new(bot).send_newspaper(newspaper: @newspaper, issue: @newspaper_issue)
  rescue Telegram::Bot::Exceptions::ResponseError => e
    if e.message.match?(/bot was blocked by the user|user is deactivated/)
      unsubscribe_user(user_telegram_id)
    else
      puts "TELEGRAM_RESPONSE_ERROR: #{e.message}"
    end
  end

  def unsubscribe_user(user_telegram_id)
    Subscription::Update.call(
      action: :unsubscribe,
      newspaper_id: @newspaper.id,
      user: User.where(telegram_id: user_telegram_id).first
    )
  end
end

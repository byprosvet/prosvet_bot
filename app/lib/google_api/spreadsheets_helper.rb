# frozen_string_literal: true

require_relative 'authorize_helper'

module GoogleApi
  class SpreadsheetsHelper
    DEFAULT_INPUT_OPTION = 'USER_ENTERED'

    def service
      @service ||= Google::Apis::SheetsV4::SheetsService.new.tap do |s|
        s.authorization = GoogleApi::AuthorizeHelper.call
      end
    end

    def read_spreadsheet(spreadsheet_id, range)
      response = service.get_spreadsheet_values(spreadsheet_id, range)
      response.values
    end

    def append_values(spreadsheet_id, range, values, value_input_option = DEFAULT_INPUT_OPTION)
      value_range = Google::Apis::SheetsV4::ValueRange.new(values: values)
      service.append_spreadsheet_value(
        spreadsheet_id, range, value_range, value_input_option: value_input_option
      )
    end

    def update_values(spreadsheet_id, range, values, value_input_option = DEFAULT_INPUT_OPTION)
      value_range = Google::Apis::SheetsV4::ValueRange.new(values: values)
      service.update_spreadsheet_value(
        spreadsheet_id, range, value_range, value_input_option: value_input_option
      )
    end
  end
end

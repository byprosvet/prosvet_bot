# frozen_string_literal: true

require 'aws-sdk-dynamodb'
require 'googleauth/token_store'

module Google
  module Auth
    module Stores
      class DynamodbTokenStore < Google::Auth::TokenStore
        def initialize(options = {}) # rubocop:disable Lint/MissingSuper
          @table_name = options.delete(:table_name)
          @dynamodb = Aws::DynamoDB::Client.new(options)
        end

        def load(id)
          params = { table_name: @table_name, key: { id: id } }
          @dynamodb.get_item(params).item&.fetch('token_value')
        end

        def store(id, token)
          params = {
            table_name: @table_name,
            key: { id: id },
            update_expression: 'set token_value = :t',
            expression_attribute_values: { ':t' => token },
            return_values: 'UPDATED_NEW'
          }
          @dynamodb.update_item(params)
        end

        def delete(id)
          params = { table_name: @table_name, key: { id: id } }
          @dynamodb.delete_item(params)
        end

        def create_table
          @dynamodb.create_table(table_scheme)
        end

        def table_scheme
          {
            table_name: @table_name,
            key_schema: [{ attribute_name: 'id', key_type: 'HASH' }],
            attribute_definitions: [{ attribute_name: 'id', attribute_type: 'S' }],
            provisioned_throughput: { read_capacity_units: 1, write_capacity_units: 1 }
          }
        end
      end
    end
  end
end

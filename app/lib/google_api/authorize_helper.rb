# frozen_string_literal: true

require 'google/apis/sheets_v4'
require 'googleauth'
require_relative 'dynamodb_token_store'

module GoogleApi
  module AuthorizeHelper
    OOB_URI = 'urn:ietf:wg:oauth:2.0:oob'
    SCOPE = [Google::Apis::SheetsV4::AUTH_SPREADSHEETS].freeze
    STORE_TABLE_NAME = 'prosvet_bot_google_auth'

    module_function

    def call
      client_id = Google::Auth::ClientId.from_hash(JSON.parse(ENV['GOOGLE_CREDENTIALS']))
      token_store = Google::Auth::Stores::DynamodbTokenStore.new(dynamodb_store_options)
      authorizer = Google::Auth::UserAuthorizer.new(client_id, SCOPE, token_store)
      user_id = 'default'
      authorizer.get_credentials(user_id) || authorize_by_url(user_id, authorizer)
    end

    def dynamodb_store_options
      options = { table_name: STORE_TABLE_NAME }
      options[:endpoint] = DYNAMODB_ENDPOINT if DYNAMODB_ENDPOINT

      options
    end

    def authorize_by_url(user_id, authorizer)
      url = authorizer.get_authorization_url(base_url: OOB_URI)
      puts 'Open the following URL in the browser and enter the ' \
           "resulting code after authorization:\n" + url
      code = gets
      authorizer.get_and_store_credentials_from_code(
        user_id: user_id, code: code, base_url: OOB_URI
      )
    end
  end
end

# frozen_string_literal: true

class Subscription
  include Dynamoid::Document

  belongs_to :user
  belongs_to :newspaper

  def self.search(user_id:, newspaper_id:)
    where('user_ids.contains' => user_id, 'newspaper_ids.contains' => newspaper_id).first
  end

  def user_telegram_id
    user_ids.first
  end
end

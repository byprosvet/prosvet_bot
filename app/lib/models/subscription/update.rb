# frozen_string_literal: true

class Subscription::Update # rubocop:disable Style/ClassAndModuleChildren
  SUBSCRIPTION_ACTIONS = %w[subscribe unsubscribe].freeze
  UnknownSubscriptionAction = Class.new(StandardError)

  class << self
    def call(action:, newspaper_id:, user:)
      raise UnknownSubscriptionAction unless SUBSCRIPTION_ACTIONS.include?(action.to_s)

      subscription = Subscription.search(user_id: user.telegram_id, newspaper_id: newspaper_id)

      if action.to_s == 'subscribe'
        subscribe(subscription, newspaper_id, user)
      else
        unsubscribe(subscription)
      end
    end

    private

    def subscribe(subscription, newspaper_id, user)
      newspaper = Newspaper.where(id: newspaper_id).first

      if newspaper.present? && subscription.blank?
        subscription = Subscription.create(user: user, newspaper: newspaper)
        result = subscription ? :subscribed : :error

        { result: result, subscription: subscription }
      else
        { result: :error, subscription: subscription }
      end
    end

    def unsubscribe(subscription)
      if subscription&.destroy
        { result: :unsubscribed, subscription: nil }
      else
        { result: :error, subscription: subscription }
      end
    end
  end
end

# frozen_string_literal: true

class User
  include Dynamoid::Document

  table key: :telegram_id

  field :telegram_id, :integer
  field :region, :string
  field :used_bot_at, :datetime
  field :role, :string

  def self.find_or_create(telegram_id)
    return if telegram_id.blank?

    User.where(telegram_id: telegram_id).first || User.create!(telegram_id: telegram_id)
  end

  def admin?
    role == 'admin'
  end
end

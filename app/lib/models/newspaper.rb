# frozen_string_literal: true

class Newspaper
  include Dynamoid::Document

  field :title, :string
  field :search_title, :string
  field :active, :boolean, default: true
  field :locations, :array, of: :string
  field :description, :string
  field :color, :string
  field :order, :integer
  field :add_to_map, :boolean, default: true
  field :key, :string

  has_many :newspaper_issues

  def self.active
    where(active: true)
  end

  def self.search(newspaper_title)
    where(search_title: newspaper_title.downcase.squeeze(' ').strip).first
  end

  def self.by_region(region)
    active.where('locations.contains': region)
  end

  def self.newspaper_issues_data(region: nil)
    newspapers = region ? by_region(region) : active

    data = newspapers.map do |newspaper|
      issue = newspaper.find_actual_issue
      next unless issue

      { newspaper: newspaper, issue: issue }
    end

    data.compact
  end

  def self.create_or_update_by_key(params)
    attrs = params.merge(search_title: params['title'].downcase).compact
    where(key: attrs['key']).first&.tap { |newspaper| newspaper.update_attributes(attrs) } || create(attrs)
  end

  def find_actual_issue
    newspaper_issues.max_by { |i| [i.date, i.version] }
  end
end

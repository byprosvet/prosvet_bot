# frozen_string_literal: true

class NewspaperIssue
  include Dynamoid::Document

  field :telegram_file_id, :string
  field :number, :integer
  field :file_name, :string
  field :date, :date
  field :version, :integer

  belongs_to :newspaper

  def self.search(number, date, version = nil)
    search_params = { number: number, date: date, version: version }.compact

    where(search_params).first
  end

  def add_to_map?
    newspaper.add_to_map
  end
end

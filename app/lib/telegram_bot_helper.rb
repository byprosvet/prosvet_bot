# frozen_string_literal: true

class TelegramBotHelper
  TELEGRAM_BOT_TOKEN = ENV['TELEGRAM_BOT_TOKEN']
  MESSAGE_DEFAULT_OPTIONS = { parse_mode: 'HTML', disable_web_page_preview: true }.freeze
  REPLY_KEYBOARD_DEFAULT_OPTIONS = { one_time_keyboard: true, resize_keyboard: true }.freeze

  def self.api
    Telegram::Bot::Api.new(TELEGRAM_BOT_TOKEN)
  end

  attr_reader :api, :chat_id

  def initialize(chat_id)
    @chat_id = chat_id
    @api = self.class.api
  end

  def show_menu(text_key, button_keys)
    show_reply_keyboard(I18n.t("texts.#{text_key}"), keyboard_for(button_keys))
  end

  def keyboard_for(button_keys)
    button_keys.map { |button_key| [I18n.t("buttons.#{button_key}")] }
  end

  def reply_keyboard_for(button_keys)
    build_reply_keyboard(keyboard_for(button_keys))
  end

  def build_reply_keyboard(keyboard, options = {})
    keyboard_options = REPLY_KEYBOARD_DEFAULT_OPTIONS.merge(options).merge(keyboard: keyboard)

    Telegram::Bot::Types::ReplyKeyboardMarkup.new(keyboard_options)
  end

  def show_reply_keyboard(text, keyboard)
    send_message(text: text, reply_markup: build_reply_keyboard(keyboard))
  end

  def send_document(options)
    api.send_document(default_message_options.merge(options))
  end

  def send_message(options)
    api.send_message(default_message_options.merge(options))
  end

  def default_message_options
    MESSAGE_DEFAULT_OPTIONS.merge(chat_id: @chat_id).compact
  end

  def notify_issue_status(result)
    issue = result[:issue]
    msg_data = {
      title: issue&.newspaper&.title,
      number: issue&.number,
      date: issue&.date,
      version: issue&.version
    }.compact
    key = (result[:error] || result[:message]).to_s
    text = I18n.t("texts.issue_status.#{key}", Hash(msg_data))

    send_message(text: text)
  end

  def subscribe_reply_markup(subscription, newspaper_id)
    action = subscription.present? ? :unsubscribe : :subscribe
    keyboard_options = { text: I18n.t("texts.#{action}"), callback_data: [action, newspaper_id].join('_') }
    keyboard = [Telegram::Bot::Types::InlineKeyboardButton.new(keyboard_options)]

    Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: keyboard)
  end

  def method_missing(method_name, *args, &block)
    api.respond_to?(method_name) ? api.public_send(method_name, *args) : super
  end

  def respond_to_missing?(*args)
    api.respond_to?(args[0]) || super
  end
end

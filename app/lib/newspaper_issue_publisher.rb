# frozen_string_literal: true

class NewspaperIssuePublisher
  DATA_SPREADSHEET_ID = ENV['DATA_SPREADSHEET_ID']
  DOCUMENTS_SHEET = ENV['DOCUMENTS_SHEET']

  attr_reader :newspaper_issue

  def initialize(newspaper_issue)
    @newspaper_issue = newspaper_issue
  end

  def call
    add_to_spreadsheet
  end

  private

  def add_to_spreadsheet
    row = [link_text, download_link, newspaper.color, 1, nil, newspaper.order]
    options = [DATA_SPREADSHEET_ID, DOCUMENTS_SHEET, [row]]

    GoogleApi::SpreadsheetsHelper.new.append_values(*options)
  end

  def download_link
    file = FileService::TelegramDownloader.call(newspaper_issue.telegram_file_id, newspaper_issue.file_name)
    FileService::S3Uploader.new.upload(file)&.public_url
  end

  def link_text
    "#{newspaper_issue.newspaper.title} (Выпуск #{newspaper_issue.number})"
  end

  def newspaper
    @newspaper ||= @newspaper_issue.newspaper
  end
end

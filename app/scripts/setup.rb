# frozen_string_literal: true

require_relative 'seeds'
require_relative 'create_tables'

module Setup
  module_function

  def call
    create_tables if ENV['CREATE_TABLES']
    seed_data if ENV['SEED_DATA']
    setup_google_auth if ENV['SETUP_GOOGLE_AUTH'] && ENV['GOOGLE_CREDENTIALS']
  end

  def create_tables
    CreateTables.call
  end

  def seed_data
    Seeds.call
  end

  def setup_google_auth
    GoogleApi::AuthorizeHelper.call
  end
end

Setup.call

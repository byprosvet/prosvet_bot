# frozen_string_literal: true

require 'json'
require 'net/http'
require 'telegram/bot'

module SetBotWebhook
  NGROK_API_HOST = 'http://localhost:4040'
  NGROK_TUNNELS_ENDPOINT = '/api/tunnels'
  LAMBDA_WEBHOOK_ENDPOINT = '/webhook'
  LAMBDA_FUNCTION_NAME = 'ProsvetBotFunction'

  module_function

  def call
    telegram_bot_token = ENV['TELEGRAM_BOT_TOKEN'] || telegram_bot_token_from_file
    webhook_url = ENV['WEBHOOK_URL'] || webhook_url_from_ngrok

    puts "TELEGRAM_BOT_TOKEN = #{telegram_bot_token}"
    puts "webhook_url = #{webhook_url}"
    puts 'Are you sure you want to set webbhook? (y/N)'
    return puts 'Exiting' if gets.strip != 'y'

    set_webhook(telegram_bot_token, webhook_url).tap { |result| puts(result || 'Cant set webhook') }
  end

  def set_webhook(telegram_bot_token, webhook_url)
    return unless telegram_bot_token && webhook_url

    api = Telegram::Bot::Api.new(telegram_bot_token)
    api.delete_webhook
    api.set_webhook(url: webhook_url)
  end

  def telegram_bot_token_from_file(filepath = 'env.json')
    file_data = File.read(filepath)
    json = JSON.parse(file_data)
    json[LAMBDA_FUNCTION_NAME]['TELEGRAM_BOT_TOKEN']
  end

  def webhook_url_from_ngrok
    response = Net::HTTP.get(URI(NGROK_API_HOST + NGROK_TUNNELS_ENDPOINT))
    json = JSON.parse(response)
    https_tunnel = json['tunnels'].find { |tunnel| tunnel['public_url'] =~ /^https/ }
    https_tunnel['public_url'] + LAMBDA_WEBHOOK_ENDPOINT if https_tunnel
  end
end

SetBotWebhook.call

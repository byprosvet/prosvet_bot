# frozen_string_literal: true

require_relative '../app'

module CreateTables
  module_function

  def call
    create_model_tables
    create_google_auth_table
  end

  def create_model_tables
    User.create_table
    Newspaper.create_table
    NewspaperIssue.create_table
    Subscription.create_table
  end

  def create_google_auth_table(options = GoogleApi::AuthorizeHelper.dynamodb_store_options)
    Google::Auth::Stores::DynamodbTokenStore.new(options).create_table
  rescue Aws::DynamoDB::Errors::ResourceInUseException # rubocop:disable Lint/SuppressedException
  end
end

# frozen_string_literal: true

require_relative '../app'

module Seeds
  module_function

  def call
    data.map { |newspaper| Newspaper.create_or_update_by_key(newspaper) }
  end

  def data
    YAML.load_file('data/seeds.yml')
  end
end

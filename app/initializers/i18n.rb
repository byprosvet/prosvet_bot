# frozen_string_literal: true

require 'i18n'

I18n.available_locales = %i[ru]
I18n.load_path << Dir["#{File.expand_path('data/locales')}/*.yml"]
I18n.default_locale = :ru

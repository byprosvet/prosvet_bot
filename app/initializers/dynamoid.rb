# frozen_string_literal: true

require 'dynamoid'

DYNAMODB_ENDPOINT = if ENV['DYNAMO_ENDPOINT']
                      ENV['DYNAMO_ENDPOINT']
                    elsif ENV['AWS_SAM_LOCAL']
                      'http://dynamodb:8000'
                    end

Dynamoid.config.endpoint = DYNAMODB_ENDPOINT if DYNAMODB_ENDPOINT
Dynamoid.config.namespace = 'prosvet_bot'
